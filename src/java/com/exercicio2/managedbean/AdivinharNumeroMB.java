/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exercicio2.managedbean;

import java.util.Random;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author comp2
 */
@Named(value = "adivinharNumeroMB")
@ViewScoped
public class AdivinharNumeroMB {

    private Integer tentativas;
    private Integer number;
    private Integer secretNumber;
    private String msg;
    private Integer fase;
    private Integer acertos;

    public Integer getFase() {
        return fase;
    }

    public void setFase(Integer fase) {
        this.fase = fase;
    }

    public Integer getAcertos() {
        return acertos;
    }

    public void setAcertos(Integer acertos) {
        this.acertos = acertos;
    }

    public Integer getTentativas() {
        return tentativas;
    }

    public void setTentativas(Integer tentativas) {
        this.tentativas = tentativas;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getSecretNumber() {
        return secretNumber;
    }

    public void setSecretNumber(Integer secretNumber) {
        this.secretNumber = secretNumber;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public void tentar() {
        if (tentativas == 0) {
            setMsg("Fim de jogo, você não pssui mais tentativas, o número era: " + secretNumber);
            return;
        }
        if (secretNumber.equals(number)) {
            setMsg("Parabéns!! Você acertou.");
            Random random = new Random();
            secretNumber = random.nextInt(1000000) / 10000;
            System.out.println("Number: " + secretNumber);
            number = null;
            tentativas = 11 - fase;
            acertos++;
        } else if (number < 0 || number > 100) {
            setMsg("O número digitado deve ser entre 0 e 100.");
        } else if (secretNumber.compareTo(number) < 0) {
            setMsg("O número é menor que " + number);
            tentativas--;
        } else if (secretNumber.compareTo(number) > 0) {
            setMsg("O número é maior que " + number);
            tentativas--;
        }
        switch (fase) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7: {
                if (acertos == 3) {
                    fase++;
                    tentativas = 11 - fase;
                    acertos = 0;
                    setMsg("Parabéns, Você passou para fase " + fase + ".");
                }
            }
            break;
            case 8:
            case 9: {
                if (acertos == 2) {
                    acertos = 0;
                    fase++;
                    tentativas = 11 - fase;
                    setMsg("Parabéns, Você passou para fase " + fase + ".");
                }
            }
            break;
            case 10: {
                setMsg("Parabéns, Você finalizou o jogo.");
            }
            default:
        }
    }

    public void novoJogo() {
        Random random = new Random();
        secretNumber = random.nextInt(1000000) / 10000;
        tentativas = 10;
        number = null;
        acertos = 0;
        fase = 1;
        System.out.println("Number: " + secretNumber);
    }

    /**
     * Creates a new instance of AdivinharNumeroMB
     */
    public AdivinharNumeroMB() {
    }

}
